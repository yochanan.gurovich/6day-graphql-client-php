<?php
/*
Queries are called using Query method, to satisfy senging mulitple querues at once
Mutations are called one at a time, sent to __call method.
*/

namespace SixDay\Client;

class Client
{
    private const URL = 'https://6day.in/graphql';
    private $client_id;

    public function __construct(array $_config_ = [])
    {
        if (isset($_config_['client_id'])) {
            $this->setClientId($_config_['client_id']);
        }
    }

    public function setClientId(string $client_id)
    {
        $this->client_id = $client_id;
    }

    public function Query($_config_)
    {
        $_config2_ = [
            'params' => [
                'query' => ''
            ]
        ];
        foreach ($_config_['queries'] ?? [] as $k => $v) {
            $_config2_['params']['query'] .= PHP_EOL
                . static::buildQuery(
                    $this->buildQueryBuilderConfig($k, $v)
                ) . PHP_EOL;
        }
        $_config2_['params']['query'] = '{' . $_config2_['params']['query'] . '}';
        return self::parseResponse(self::Request($_config2_['params']));
    }

    public function __call(string $name, array $arguments)
    {
        $_raw_response_ = self::Request(
            [
                'query' => 'mutation{' . static::buildQuery($this->buildQueryBuilderConfig($name, $arguments[0])) . "}"
            ]
        );
        if (FALSE) {
            if ($name == 'createOrder') { /* calculateSalesTax */
                print_var($_raw_response_);
                exit();
            }
        }
        $_response_ =  self::parseResponse($_raw_response_);
        return $_response_;
    }

    private function buildQueryBuilderConfig(string $name, array $_config_)
    {
        $_method_ = "buildQueryBuilderConfig_$name";
        if (method_exists(get_called_class(), $_method_)) {
            return $this->$_method_($name, $_config_);
        }
        return [
            'name' => $name,
            'args' => $_config_['args'] ?? [],
            'fields' => $_config_['fields'] ?? []
        ];
    }

    private function buildQueryBuilderConfig_createToken(string $name, array $_config_)
    {
        return [
            'name' => $name,
            'args' => array_merge(
                [
                    'client_id' => $this->client_id
                ],
                $_config_['args'] ?? []
            )
        ];
    }

    private function buildQueryBuilderConfig_createPasswordResetRequest(string $name, array $_config_)
    {
        return [
            'name' => $name,
            'args' => array_merge(
                [
                    'client_id' => $this->client_id
                ],
                $_config_['args'] ?? []
            ),
            'fields' => $_config_['fields'] ?? []
        ];
    }

    static private function buildQuery($_config_)
    {
        return $_config_['name']
            . (!empty($_config_['args']) ? '(' . self::buildArgs($_config_['args']) . ')' : '')
            . (!empty($_config_['fields']) ? '{' . PHP_EOL . self::buildFields($_config_['fields']) . PHP_EOL . '}' : '');
    }

    static private function buildArgs(array $_list_)
    {
        $_s_ = '';
        foreach ($_list_ as $k => $v) {
            if (is_array($v)) {
                if (array_is_list($v)) {
                    $_value_ = '[' . PHP_EOL . self::buildArgs($v) . ']';
                } else {
                    $_value_ = '{' . PHP_EOL . self::buildArgs($v) . '}';
                }
            } else {
                $_value_ = json_encode($v);
            }

            if (array_is_list($_list_)) {
                $_s_ .= $_value_ . PHP_EOL;
            } else {
                $_s_ .= "$k: $_value_" . PHP_EOL;
            }
        }
        return $_s_;
    }

    static private function buildFields(array $_list_)
    {
        $_s_ = '';
        foreach ($_list_ as $k => $v) {
            if (is_object($v)) {
                $_s_ .= $k;
                if (!empty($v->args)) {
                    $_s_ .= '(' . self::buildArgs($v->args) . ')';
                }
                if (!empty($v->fields)) {
                    $_s_ .= ' {' . PHP_EOL . self::buildFields($v->fields) . '}' . PHP_EOL;
                }
            } elseif (is_array($v)) {
                $_s_ .= "$k {" . PHP_EOL . self::buildFields($v) . '}' . PHP_EOL;
            } else {
                $_s_ .= $v . PHP_EOL;
            }
        }
        return $_s_;
    }

    static private function Request(array $_params_)
    {
        $_curl_ = curl_init();
        curl_setopt($_curl_, CURLOPT_URL, self::URL);
        curl_setopt($_curl_, CURLOPT_POST, TRUE);
        curl_setopt($_curl_, CURLOPT_POSTFIELDS, http_build_query($_params_));
        curl_setopt($_curl_, CURLOPT_RETURNTRANSFER, TRUE);
        $_response_ = curl_exec($_curl_);
        curl_close ($_curl_);
        return $_response_;
    }

    static private function parseResponse(string $_raw_response_)
    {
        $_response_ = json_decode($_raw_response_);
        $_a_ = [];
        if (!empty($_response_->errors)) {
            foreach ($_response_->errors as $v) {
                $_key_ = $v->path[0] ?? 0;
                if (!isset($_a_[$_key_])) {
                    $_a_[$_key_] = (object)[
                        'success' => FALSE,
                        'errors' => []
                    ];
                }
                $_a_[$_key_]->errors[] = (object)[
                    'message' => $v->message,
                    'props' => $v->props ?? []
                ];
            }
        }
        foreach ($_response_->data ?? [] as $k => $v) {
            if (isset($_a_[$k])) {
                continue;
            }
            $_a_[$k] = (object)[
                'success' => TRUE,
                'data' => $v
            ];
        }
        return $_a_;
    }
}
